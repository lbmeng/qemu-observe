# QEMU 模拟器观察

即时观察 QEMU 新版本的关键特性

## 快速构建

```
$ git clone https://gitee.com/tinylab/cloud-lab
$ cd cloud-lab
$ tools/docker/run markdown-lab
(markdown-lab) git clone https://gitee.com/tinylab/qemu-observe && cd qemu-observe
(markdown-lab) make clean
(markdown-lab) make
(markdown-lab) make v7.2/v7.2.pdf
```

## 注意事项

- 头图
    - 检索顺序：
        - images/v7.2/v7.2_titlepage_wallpaper.png
        - images/v7.2/v7.2.png
        - images/v7.2/titlepage_wallpaper.png
        - common/titlepage_wallpaper.png
    - 支持格式：png 或 jpg

- 列表
    - 列表缩进：4 个空格
    - 列表级数：不超过 3 级

- 图片
    - 图片尺寸控制，请用 Latex 语法
    - 见 [v6.0/v6.0.md](https://gitee.com/tinylab/linux-observe/tree/master/v6.0) 中 `\begin{figure}[h]` 示例，可用 `height` 或 `width` 控制宽高

- 超长页
    - 内容过多，可以另起一页，用同样标题，加（Cont.）后缀

## 相关资源

* 协作仓库：<https://gitee.com/tinylab/qemu-observe>
* 实验环境：<https://tinylab.org/linux-lab-disk>
* 关联活动：<https://gitee.com/tinylab/riscv-linux>

## 活动地址

在线直播地址请关注 B 站动态。

* 泰晓学院：<https://www.cctalk.net/m/group/90483396>
* B 站账号：<https://space.bilibili.com/687228362>
* 知乎专栏：<https://www.zhihu.com/column/tinylab>

## 联系我们

可联系加入『QEMU 模拟器观察』技术讨论群。

* 微信号：tinylab
* 邮箱地址：contact@tinylab.org
