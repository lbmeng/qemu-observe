
# QEMU Observe

Observe the new features of the latest QEMU versions.

## Build

```
$ git clone https://gitee.com/tinylab/cloud-lab
$ cd cloud-lab
$ tools/docker/run markdown-lab
(markdown-lab) git clone https://gitee.com/tinylab/qemu-observe && cd qemu-observe
(markdown-lab) make clean
(markdown-lab) make
(markdown-lab) make v7.2/v7.2.pdf
```

## Resources

* Repository: <https://gitee.com/tinylab/qemu-observe>
* Environment: <https://tinylab.org/linux-lab-disk>
* Related: <https://gitee.com/tinylab/riscv-linux>

## Where

* Community: <https://www.cctalk.net/m/group/90483396>
* Bilibili: <https://space.bilibili.com/687228362>
* Zhihu: <https://www.zhihu.com/column/tinylab>

## Contact

* Wechat: tinylab
* Email: contact@tinylab.org
