# Author: Wu Zhangjin / Falcon <falcon@tinylab.org>
# Update: 2014/01/17 PM 14:20, 2022/08/15 AM 2:53

SRC = $(wildcard v*/*.md)

html_theme=dzslides
latex_theme=Darmstadt

# colortheme: beaver,crane,dolphin,dove,fly,lily
# monarca,seagull,seahorse,spruce,wolverine
latex_colortheme=wolverine

# configure the ratio of width/height
latex_aspectratio=169

# Put and select the .jpg's in images/, without suffix
#titlepage_wallpaper=
titlepage_titlefg=white
titlepage_authorfg=darkgray

# toc bg/fg
toc_number_bg=white
toc_number_fg=violet
toc_section_bg=white
toc_section_fg=orange

latex_fontsize=9pt
latex_template=./templates/zh_template.tex

latex_pagefoot="欢迎加入『QEMU 模拟器观察』小组，联系微信：tinylab，公众号：泰晓科技"

# pygments / kate /monochrome /espresso /haddock /tango /zenburn
highlight_style ?= espresso

PDFS=$(SRC:.md=.pdf)
HTML=$(SRC:.md=.html)
IMGS=$(wildcard images/*.jpg)

all: clean $(PDFS) $(HTML)

pdf: $(PDFS)

html: $(HTML)

%.pdf: %.md $(latex_template) $(IMGS)
	sed '/^# 致谢/,$$d' $< > $<.tmp
	pandoc -t beamer --toc --listings \
	  --latex-engine=xelatex \
	  --template=$(latex_template) \
          $(if $(latex_aspectratio),-V aspectratio=$(latex_aspectratio)) \
	  -V titlepage_wallpaper:$$(for d in $(basename $<)_titlepage_wallpaper $(basename $<) $(dir $<)titlepage_wallpaper common/titlepage_wallpaper; do if [ -f "images/$$d.png" -o -f "images/$$d.jpg" ]; then echo "$$d"; break; fi; done) \
	  -V titlepage_titlefg:$(titlepage_titlefg) \
	  -V titlepage_authorfg:$(titlepage_authorfg) \
	  -V toc_number_bg:$(toc_number_bg) \
	  -V toc_number_fg:$(toc_number_fg) \
	  -V toc_section_bg:$(toc_section_bg) \
	  -V toc_section_fg:$(toc_section_fg) \
	  -V colortheme:$(latex_colortheme) \
	  -V theme:$(latex_theme) \
	  -V fontsize:$(latex_fontsize) \
	  -V pagefoot:$(latex_pagefoot) \
	  $<.tmp -o $@
	rm $<.tmp

%.html: %.md
	pandoc -t $(html_theme) -s --mathjax $< -o $@
	sed -i -e "s/width: 800px; height: 600px;$$/width: 1280px; height: 768px;/" $@
	sed -i -e "s/margin-left: -400px; margin-top: -300px;$$/margin-left: -640px; margin-top: -384px;/" $@

read: $(PDFS)
	(chromium-browser $^ 2>&1 > /dev/null &) 2>&1 >/dev/null

view:
	evince $(PDFS)

read-html: $(HTML)
	(chromium-browser $^ 2>&1 > /dev/null &) 2>&1 >/dev/null

clean:
	rm -rf $(PDFS) $(HTML) *.tmp
